import { createApp ,h } from 'vue'
import App from './App.vue'
import Deneme from './components/deneme.vue'
import databinding from './components/databinding.vue'
import changeclass from './components/changeclass.vue'


const routes = {
  '/about': Deneme,
  '/':App,
  '/deneme.page' : App,
  '/databinding' : databinding,
  '/changeclass' : changeclass,

}

const SimpleRouter = {
  data: () => ({
    currentRoute: window.location.pathname
  }),

  computed: {
    CurrentComponent() {
      return routes[this.currentRoute] || App 
    }
  },

  render() {
    return h(this.CurrentComponent)
  }
}



const app = createApp(SimpleRouter).mount('#app')

